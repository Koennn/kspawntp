# KSpawnTP plugin #
Get teleported to spawn when you log in.

Warning! Requires KoennCore to be installed.


**[Click here to download the plugin](http://download.koenn.nl/KSpawnTP.jar)**

**[Click here to download KoennCore](http://download.koenn.nl/KoennCore.jar)**



```
commands:
  ksetspawn:
    usage: /ksetspawn
    description: Set the hub spawn location.


permissions:
  kspawn.setspawn:
    description: Use the /ksetspawn command.
```