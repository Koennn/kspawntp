package me.koenn.spawntp.commands;

import me.koenn.core.command.Command;
import me.koenn.core.misc.LocationHelper;
import me.koenn.core.player.CPlayer;
import me.koenn.spawntp.KSpawnTP;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class SetSpawnCommand extends Command {

    /**
     * Register the SetSpawn command.
     */
    public SetSpawnCommand() {
        super("ksetspawn", "/ksetspawn");
    }

    /**
     * Gets called whenever the SetSpawn command is executed.
     *
     * @param player Command executor
     * @param args   Command arguments
     * @return boolean handled
     */
    @Override
    public boolean execute(CPlayer player, String[] args) {
        //Check if the player has the right permissions.
        if (!player.getPlayer().hasPermission("kspawn.setspawn")) {

            //If not, send an error message.
            player.sendMessage(ChatColor.RED + "You do not have permission to use this command.");

            //Stop handling the command.
            return true;
        }

        //Set the spawnLocation variable to the player's location.
        Location spawnLocation = player.getPlayer().getLocation();

        //Set the locationString variable to the formatted location.
        String locationString = LocationHelper.getString(spawnLocation, true);

        //Save the locationString to the data file using the dataManager.
        KSpawnTP.getDataManager().setInBody("hubSpawn", locationString);

        //Send a message to the player.
        player.sendMessage(ChatColor.GREEN + "Successfully saved the new spawn location!");
        return true;
    }
}
