package me.koenn.spawntp;

import me.koenn.core.command.CommandAPI;
import me.koenn.core.data.JSONManager;
import me.koenn.spawntp.commands.SetSpawnCommand;
import me.koenn.spawntp.listeners.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class KSpawnTP extends JavaPlugin {

    /**
     * Static instance of this plugin class.
     */
    private static Plugin instance;

    /**
     * JSON data manager for the plugin.
     */
    private static JSONManager dataManager;

    /**
     * Get the instance of this class.
     *
     * @return Plugin instance
     */
    public static Plugin getInstance() {
        return instance;
    }

    /**
     * Get JSON data manager.
     *
     * @return JSONManager object
     */
    public static JSONManager getDataManager() {
        return dataManager;
    }

    /**
     * Simple logger method to log a message to the console.
     *
     * @param msg Message to log
     */
    public void log(String msg) {
        this.getLogger().info(msg);
    }

    /**
     * Plugin enable method.
     * Gets called when the plugin enables
     */
    @Override
    public void onEnable() {
        //Set static variables.
        instance = this;

        //Send credit message.
        log("All credits for this plugin go to Koenn");

        //Check the Bukkit version and disable the plugin if its invalid.
        if (!checkVersion()) {
            log("#####################################################");
            log("This plugin is only compatible with 1.8, 1.9 and 1.10");
            log("#####################################################");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        //Register the Bukkit event Listeners.
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);

        //Register the Commands.
        CommandAPI.registerCommand(new SetSpawnCommand(), this);

        //Create the JSON data manager.
        dataManager = new JSONManager(this, "data.json");
    }

    /**
     * Get the current Bukkit version of the server and check if its valid.
     */
    public boolean checkVersion() {
        //Check the version and set the version variable.
        String versionString = Bukkit.getServer().getBukkitVersion();
        String version;
        if (versionString.contains("1.9")) {
            version = "1.9";
        } else if (versionString.contains("1.8")) {
            version = "1.8";
        } else if (versionString.contains("1.10")) {
            version = "1.10";
        } else {
            return false;
        }
        log("Loading plugin for minecraft version " + version + "!");
        return true;
    }

    /**
     * Plugin disable method.
     * Gets called when the plugin disables.
     */
    @Override
    public void onDisable() {
        log("All credits for this plugin go to Koenn");
    }
}