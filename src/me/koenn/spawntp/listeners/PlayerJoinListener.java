package me.koenn.spawntp.listeners;

import me.koenn.core.misc.LocationHelper;
import me.koenn.core.misc.Timer;
import me.koenn.spawntp.KSpawnTP;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class PlayerJoinListener implements Listener {

    /**
     * Listener for the PlayerJoinEvent.
     *
     * @param event PlayerJoinEvent object
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        //Make the spawn variable.
        Location spawn;

        //Try to set the spawn variable to the hub spawn location.
        try {
            spawn = LocationHelper.fromString((String) KSpawnTP.getDataManager().getFromBody("hubSpawn"), true);
        } catch (NullPointerException ex) {

            //If the hub spawn location is not set, stop executing.
            return;
        }

        //Start a timer for 1 second.
        new Timer(20, KSpawnTP.getInstance()).start(() -> {

            //Teleport the player to the spawn location.
            event.getPlayer().teleport(spawn);
        });
    }
}
